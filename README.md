# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|edilaine de lima -|edilima
|tais remussi
|-lucas de oliveira santos-|

# Documentação

A documentação do projeto pode ser acessada pelo link:

> inserir o link da página de documentação.

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)